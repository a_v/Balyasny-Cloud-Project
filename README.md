# Balyasny-Cloud-Project

## Setting up

Need to install Python on your machine. Your milage may very depending on os, below are some download links to the most popular os’s

[Python Download for Windows](https://www.python.org/downloads/windows/)

[Python Download for OSX](https://www.python.org/downloads/mac-osx/)

## Configuration

You will need to add your AWS Secret/Access Keys and region in the configuration section or by using environment variables.  If you open vpc_better.py  you will see a configuration variables portion.

```python
###Configuraiton Variables###
aws_acess_key = ''
aws_secret_key = ''
region = 'us-west-2'
subnets = ['192.168.1.0/24']
az = ['us-west-2a']
cidr_block = '192.168.0.0/16'
ami_id = 'ami-1e299d7e'
ec2_key_name = 'BAM-MINI-PROJECT'
instance_type = 't2.micro'
```

## Running The Thing

You will need to jump into the folder that was cloned and run the script by using the below command. Make sure you have your key’s in the right place or you’ll likely get errors.

```python
python BAM-Mini-Project.py
```

## What To Expect

This script will create an ec2 instances with its supporting surrounding infrastructure that will render a webpage at the end that says “AWS Automation is Fun!”.

This will check/create:

1. Check if VPC is there by checking tags and cidr. The program will stop if the name and cidr  are matched.
2. Create a VPC and enable public dns.
3. Create Subnet(s).
4. Create a Internet Gateway and attach it to a VPC.
5. Will point 0.0.0.0/0 to the Above Internet Gateway in the route table.
6. Create two security groups that will allow ssh and http.
7. Will create a private key for you to use to connect to the instances for troubleshooting and print that out on the screen.
8. Will wait for the instances to initialize so we can grab the public dns. 

### Expected Results

```python
python BAM-Mini-Project.py
This VPC is Not here
Created VPC
Created Subnet(s): ['192.168.1.0/24']
Created Internet Gateway
Pointing 0.0.0.0/0 to Created Internet Gateway in Route Table
Created HTTP Security Group
Created SSH Security Group
New Private Key created,Save the below key-material

-----BEGIN RSA PRIVATE KEY-----
omitted
-----END RSA PRIVATE KEY-----
Waiting for Instances to come up for Public DNS
ec2-54-218-79-224.us-west-2.compute.amazonaws.com
Wait about 1 minute and use the public dns for the web page.
```

## Things I Would Like to Do Next

There is a couple of things that I didn’t finish up and would like to come back around and do.

1. Error handling. Would love to get a little more sophisticated around errors that come up.
2. Testing the code.
3. Passing in variables from a command line statement.
